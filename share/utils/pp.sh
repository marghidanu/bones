#!/bin/bash

# --- Dependencies
apt-get install -y libperl-dev

cpanm --notest --quiet PAR::Packer \
	File::HomeDir IO::Interactive Text::WagnerFischer

# --- Packaging
export PAR_VERBATIM=1

pp blib/script/bones.pl -o bones -c \
	-I blib/lib \
	-M Bones:: \
	-M MooseX::Log::Log4perl::
