package Bones::Command::Init {
	use MooseX::App::Command;

	extends 'Bones';

	use Bones::Skeleton;
	use Bones::SkeletonFactory;

	use List::Util qw( uniq );

	use Mojo::File;

	option 'skeleton' => (
		is => 'ro',
		isa => 'ArrayRef[Str]',
		required => 1,
	);

	option 'path' => (
		is => 'ro',
		isa => 'Str',
		required => 1,
		cmd_aliases => [ qw( p ) ],
	);

	option 'author' => (
		is => 'ro',
		isa => 'Str',
		default => $ENV{USER},
		cmd_aliases => [ qw( a ) ],
	);

	option 'email' => (
		is => 'ro',
		isa => 'Maybe[Str]',
		default => undef,
		cmd_aliases => [ qw( e ) ],
	);

	option 'cache' => (
		is => 'ro',
		isa => 'Bool',
		cmd_aliases => [ qw( c ) ],
	);

	option 'overwrite' => (
		is => 'ro',
		isa => 'Bool',
		cmd_aliases => [ qw( o ) ],
	);

	with 'MooseX::Log::Log4perl';

	command_short_description( 'Run one or multiple skeletons' );

	sub run {
		my $self = shift();

		# Making sure tha cache directory always exists
		my $base = Mojo::File::path( $self->path() )
			->child( '.bones' )
			->make_path();

		foreach my $name ( uniq( @{ $self->skeleton() } ) ) {
			$self->log()->info( "Shaking the bones on the ${name} skeleton" );

			# Loading the context from a file or creating a new one.
			my $file = $base->child( $name )->to_string();
			my $context = ( $self->cache() && -e $file && -f $file ) ?
				Bones::Skeleton::Context->load( $file ) :
				Bones::Skeleton::Context->new();

			$self->log()->info( 'Cache is activated, looking for old bones for this skeleton.' )
				if( $self->cache() );

			# Even if globals are serialized we simply discard them.
			$context->globals(
				{
					path => $self->path(),
					author => $self->author(),
					email => $self->email(),
					overwrite => $self->overwrite(),
				}
			);

			# Running the skeleton and storing the context into a file.
			Bones::SkeletonFactory->create( $name )
				->run( $context )
				->store( $file );
		}

		return 0;
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
