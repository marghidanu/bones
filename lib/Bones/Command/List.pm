package Bones::Command::List {
	use MooseX::App::Command;

	extends 'Bones';

	use Module::Util qw( find_in_namespace );

	use Mojo::Loader qw( load_class );
	use Mojo::Util qw( decamelize );

	option 'verbose' => (
		is => 'ro',
		isa => 'Bool',
		cmd_aliases => [ qw( v ) ]
	);

	with 'MooseX::Log::Log4perl';

	command_short_description( 'List available skeletons' );

	sub run {
		my $self = shift();

		foreach my $class ( sort( find_in_namespace( 'Bones::Skeleton' ) ) ) {
			my $error = load_class( $class );

			$self->log()->error( "There was an error loading ${class} : ${error}" ) && next()
				if( ref( $error ) );

			$self->log()->error( "Class ${class} is not a skeleton!" )
				unless( $class->isa( 'Bones::Skeleton' ) );

			my @tasks = $class->new()->get_ordered_tasks();

			my ( $name ) = $class =~ m/^Bones::Skeleton::(.+)/x;
			printf( "%s (%d)\n", decamelize( $name ), scalar( @tasks ) );
			if( $self->verbose() ) {
				printf( "\t%s\n", $_ )
					foreach( @tasks );
			}
		}

		return 0;
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
