package Bones::Spine {
	use Moose;

	use Mojo::File qw( path );

	use Bones::Spine::Block;
	use Bones::Spine::FilterFactory;

	has 'base' => (
		is => 'ro',
		isa => 'Maybe[Str]',
		default => undef,
	);

	has 'blocks' => (
		is => 'ro',
		isa => 'ArrayRef[Bones::Spine::Block]',
		default => sub { [] },
		traits => [ qw( Array ) ],
		handles => {
			add_block => 'push',
			clear => 'clear',
		},
	);

	sub load {
		my ( $self, %data ) = @_;

		while( my ( $file, $name ) = each( %data ) ) {
			my $path = defined( $self->base() ) ?
				path( $self->base() )->child( $file ) : path( $file );

			my $content = $path->slurp();
			$name = $path->basename()
				unless( defined( $name ) );

			$self->add_block(
				Bones::Spine::Block->new(
					name => $name,
					content => $content
				)
			);
		}

		return $self;
	}

	sub apply {
		my ( $self, $name, $args ) = @_;

		my $filter = Bones::Spine::FilterFactory->create( $name, $args || {} );
		$filter->apply( $_ )
			foreach( @{ $self->blocks() } );

		$filter->done();

		return $self;
	}

	sub merge {
		my ( $self, $name, $args ) = @_;

		my $content = join( $args->{separator} || "\n",
			map { $_->content() } @{ $self->blocks() }
		);

		$self->clear();
		$self->add_block(
			Bones::Spine::Block->new(
				name => $name,
				content => $content
			)
		);

		return $self;
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
