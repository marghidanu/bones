package Bones::Dialog {
	use Moose;
	use Moose::Exporter;

	use Carp;

	use Bones::Dialog::ControlFactory;

	Moose::Exporter->setup_import_methods(
		as_is => [ qw( ask ) ],
	);

	sub ask {
		my @questions = @_;

		my $answers = {};
		foreach my $question ( @questions ) {
			my $type = delete( $question->{type} );
			carp( 'Definition has no type!' ) && next()
				unless( $type );

			my $control = Bones::Dialog::ControlFactory->create( $type, $question );
			$answers->{ $control->name() } = $control->ask();
		}

		return $answers;
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
