package Bones::Dialog::Control::Handler {
	use Moose;

	extends 'Bones::Dialog::Control';

	use Term::Prompt qw( prompt );

	has 'default' => (
		is => 'ro',
		isa => 'Str',
		default => '',
	);

	has 'handler' => (
		is => 'ro',
		isa => 'CodeRef',
		required => 1,
	);

	sub ask {
		my $self = shift();

		return prompt( 's', $self->title(), '', $self->default(), $self->handler() );
	}

	__PACKAGE__->meta()->make_immutable();
}

1;
