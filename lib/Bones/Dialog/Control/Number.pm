package Bones::Dialog::Control::Number {
	use Moose;

	extends 'Bones::Question::Control';

	use Term::Prompt qw( prompt );

	has 'default' => (
		is => 'ro',
		isa => 'Num',
		default => 0
	);

	sub ask {
		my $self = shift();

		return prompt( 'n', $self->title(), '', $self->default() );
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
