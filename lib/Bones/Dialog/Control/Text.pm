package Bones::Dialog::Control::Text {
	use Moose;

	extends 'Bones::Dialog::Control';

	use Term::Prompt qw( prompt );

	has 'default' => (
		is => 'ro',
		isa => 'Any',
		default => '',
	);

	sub ask {
		my $self = shift();

		return prompt( 'x', $self->title(), '', $self->default() );
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
