package Bones::Dialog::Control::Menu {
	use Moose;

	extends 'Bones::Dialog::Control';

	use Term::Prompt qw( prompt );

	has 'items' => (
		is => 'ro',
		isa => 'HashRef[Str]',
		default => sub { {} },
	);

	has 'allow_multi' => (
		is => 'ro',
		isa => 'Bool',
		default => 0,
	);

	has 'allow_empty' => (
		is => 'ro',
		isa => 'Bool',
		default => 0,
	);

	has 'default' => (
		is => 'ro',
		isa => 'Str',
		default => ''
	);

	sub ask {
		my $self = shift();

		my @labels = sort( keys( %{ $self->items() } ) );
		my @indexes = prompt( 'm',
			{
				prompt => $self->title(),
				title => '',
				cols => 1,
				items => \@labels,
				accept_multiple_selections => $self->allow_multi(),
				accept_empty_selection => $self->allow_empty(),
			}, '', $self->default(),
		);

		return [
			map { $self->items()->{ $labels[ $_ ] } }
				@indexes
		];
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
