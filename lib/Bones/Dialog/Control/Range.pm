package Bones::Dialog::Control::Range {
	use Moose;

	extends 'Bones::Dialog::Control';

	use Term::Prompt qw( prompt );

	has 'default' => (
		is => 'ro',
		isa => 'Num',
		default => 0,
	);

	has 'min' => (
		is => 'ro',
		isa => 'Num',
		default => 0,
	);

	has 'max' => (
		is => 'ro',
		isa => 'Num',
		default => 100
	);

	sub ask {
		my $self = shift();

		return prompt( 'r', $self->title(), '', $self->default(), $self->min(), $self->max() );
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
