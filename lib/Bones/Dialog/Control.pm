package Bones::Dialog::Control {
	use Moose;

	use MooseX::AbstractMethod;

	has 'name' => (
		is => 'ro',
		isa => 'Str',
		required => 1,
	);

	has 'title' => (
		is => 'ro',
		isa => 'Str',
		default => '',
	);

	abstract( 'ask' );

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
