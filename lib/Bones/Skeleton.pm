package Bones::Skeleton {
	use Moose;

	use Graph;

	use Carp;

	has 'tasks' => (
		is => 'ro',
		isa => 'HashRef[CodeRef]',
		default => sub { {} },
		traits => [ qw( Hash ) ],
		handles => {
			_set_task => 'set',
			_get_task => 'get',
		}
	);

	has '_graph' => (
		is => 'ro',
		isa => 'Graph',
		default => sub { Graph->new( directed => 1 ) }
	);

	sub add_task {
		my ( $self, $name, $deps, $code ) = @_;

		$self->_set_task( $name, $code );
		$self->_graph()->add_vertex( $name );

		$self->_graph()->add_edge( $_, $name )
			foreach( @{ $deps || [] } );

		return $self;
	}

	sub get_ordered_tasks {
		my $self = shift();

		return $self->_graph()
			->topological_sort();
	}

	sub run {
		my ( $self, $context ) = @_;

		croak( 'Invalid flow' )
			unless( $self->_graph()->is_dag() );

		foreach my $name ( $self->get_ordered_tasks() ) {
			my $task = $self->_get_task( $name );
			carp( "Task ${name} does not exist!" ) && next()
				unless( $task );

			my $result = $task->( $self, $context );
			$context->set_result( $name, $result );
		}

		return $context;
	}

	__PACKAGE__->meta()->make_immutable();
}

package Bones::Skeleton::Context {
	use Moose;

	use MooseX::Storage;

	with Storage( format => 'JSON', io => 'File' );

	has 'globals' => (
		is => 'rw',
		isa => 'HashRef',
		default => sub { {} },
		traits => [ qw( Hash ) ],
		handles => {
			global => 'get',
		}
	);

	has 'results' => (
		is => 'ro',
		isa => 'HashRef',
		traits => [ qw( Hash ) ],
		handles => {
			set_result => 'set',
			get_result => 'get',
		}
	);

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
