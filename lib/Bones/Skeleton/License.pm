package Bones::Skeleton::License {
	use Moose;

	extends 'Bones::Skeleton';

	use Mojo::File qw( path );

	use Bones::Util qw( dist_dir dist_file );
	use Bones::Dialog;
	use Bones::Spine;

	with 'MooseX::Log::Log4perl';

	sub BUILD {
		my $self = shift();

		$self->add_task( dialog => [],
			sub {
				my ( $s, $c ) = @_;

				my $defaults = $c->get_result( 'dialog' ) || {};

				# Listing all the license files for building the menu
				my $licenses = path( dist_dir() )
					->child( 'templates/license' )
					->list()
					->to_array();

				my $items = {
					map { $_->basename( '.txt.ep' ) => $_->to_string() }
						@{ $licenses }
				};

				# ... and asking the questions ...
				return ask(
					{
						type => 'menu',
						name => 'license',
						title => 'Choose you license',
						items => $items,
					},
					{
						type => 'text',
						name => 'author',
						title => 'Who is the author?',
						default => $defaults->{author} || $c->global( 'author' ),
					}
				);
			}
		);

		$self->add_task( default => [ qw( dialog ) ],
			sub {
				my ( $s, $c ) = @_;

				my $answers = $c->get_result( 'dialog' );

				# Provisioning the files ...
				my $spine = Bones::Spine->new()
					->load( $answers->{license}->[0] => 'LICENSE.TXT' )
					->apply( template => { args => $answers } )
					->apply( save => {
							base => $c->global( 'path' ),
							overwrite => $c->global( 'overwrite' ),
						}
					)
					->apply( status => {} );

				return 0;
			}
		);

		return 0;
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
