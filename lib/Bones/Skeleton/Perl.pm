package Bones::Skeleton::Perl {
	use Moose;

	extends 'Bones::Skeleton';

	use Mojo::Util qw( camelize class_to_file class_to_path );
	use Mojo::File qw( path );

	use Module::Util qw( module_name_parts is_valid_module_name );

	use Bones::Util qw( dist_dir );
	use Bones::Dialog;
	use Bones::Spine;

	with 'MooseX::Log::Log4perl';

	sub BUILD {
		my $self = shift();

		$self->add_task( dialog => [],
			sub {
				my ( $s, $c ) = @_;

				my $base = path( $c->global( 'path' ) );
				my $defaults = $c->get_result( 'dialog' ) || {};

				return ask(
					{
						type => 'handler',
						name => 'name',
						title => 'What is the name of this module?',
						default => $defaults->{name} || camelize( $base->basename() ),
						handler => sub { is_valid_module_name( shift() ) },
					},
					{
						type => 'text',
						name => 'version',
						title => 'Do you have a version in mind?',
						default => $defaults->{version} || '0.0.0',
					},
					{
						type => 'text',
						name => 'description',
						title => 'Let\'s put a description on it',
						default => $defaults->{description} || 'None',
					},
					{
						type => 'text',
						name => 'author',
						title => 'Who is the author?',
						default => $defaults->{author} || $c->global( 'author' ),
					}
				);
			}
		);

		$self->add_task( default => [ qw( dialog ) ],
			sub {
				my ( $s, $c ) = @_;

				my $answers = $c->get_result( 'dialog' );
				my @parts = module_name_parts( $answers->{name} );

				# Preparing the dynamic paths
				my $script = path( 'bin' )
					->child( class_to_file( $answers->{name} ) )
					->to_string();

				my $module = path( 'lib')
					->child( class_to_path( $answers->{name} ) )
					->to_string();

				my $utils = path( 'lib' )
					->child( @parts, 'Utils.pm' )
					->to_string();

				# Running the Perl skeleton spine
				my $base = path( dist_dir() )
					->child( 'templates/perl' )
					->to_string();

				my $spine = Bones::Spine->new( base => $base )
					->load(
						'script.ep' => $script,
						'module.ep' => $module,
						'utils.ep' => $utils,
						'config.ep' => 'share/config.json',
						'test.ep' => 't/01_load.t',
						'build.ep' => 'Build.PL',
						'readme.ep' => 'README.md',
						'gitignore.ep' => '.gitignore',
					)
					->apply( template => { args => $answers } )
					->apply( save => {
							base => $c->global( 'path' ),
							overwrite => $c->global( 'overwrite' ),
						}
					)
					->apply( status => {} );

				return 0;
			}
		);

		return 0;
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
