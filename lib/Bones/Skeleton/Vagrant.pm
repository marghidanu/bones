package Bones::Skeleton::Vagrant {
	use Moose;

	extends 'Bones::Skeleton';

	use Mojo::File qw( path );
	use Mojo::Template;

	use Bones::Util qw( dist_dir );
	use Bones::Dialog;
	use Bones::Spine;

	with 'MooseX::Log::Log4perl';

	sub BUILD {
		my $self = shift();

		$self->add_task( dialog => [],
			sub {
				my ( $s, $c ) = @_;
				my $defaults = $c->get_result( 'dialog' ) || {};

				my @parts = grep { $_ }
					@{ path( $c->global( 'path' ) )->to_array() };

				my $sections = path( dist_dir() )
					->child( 'templates/vagrant/sections' )
					->list()
					->to_array();

				my $items = {
					map { $_->basename( '.ep' ) => $_->to_string() }
						@{ $sections }
				};

				return ask(
					{
						type => 'text',
						name => 'box',
						title => 'Which box do you want to use?',
						default => $defaults->{box} || 'stuffo/xenial64',
					},
					{
						type => 'text',
						name => 'hostname',
						title => 'Let\'s put a hostname on the box',
						default => $defaults->{hostname} || pop( @parts ),
					},
					{
						type => 'range',
						name => 'cpus',
						title => 'How many CPUs?',
						default => $defaults->{cpus} || 1,
						min => 1,
						max => 16,
					},
					{
						type => 'range',
						name => 'memory',
						title => 'How much memory (in MB)?',
						default => $defaults->{memory} || 1024,
						min => 128,
						max => 8192,
					},
					{
						type => 'menu',
						name => 'sections',
						title => 'How should we provision the box?',
						items => $items,
						allow_multi => 1,
						allow_empty => 1,
					}
				);
			}
		);

		$self->add_task( default => [ qw( dialog ) ],
			sub {
				my ( $s, $c ) = @_;

				my $answers = $c->get_result( 'dialog' );

				# Applying templates to the sections
				my $files = Bones::Spine->new()
					->load( map { $_ => undef } @{ $answers->{sections} } )
					->apply( template => { args => $answers } )
					->blocks();

				$answers->{sections} = [ map { $_->content() } @{ $files } ];

				# ... and again with the provisioning ...
				my $base = path( dist_dir() )
					->child( 'templates/vagrant' )
					->to_string();

				# ... and writing everything down.
				my $spine = Bones::Spine->new( base => $base )
					->load( 'vagrant.ep' => 'Vagrantfile' )
					->apply( template => { args => $answers } )
					->apply( save => {
							base => $c->global( 'path' ),
							overwrite => $c->global( 'overwrite' ),
						}
					)
					->apply( status => {} );

				return 0;
			}
		);

		return 0;
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
