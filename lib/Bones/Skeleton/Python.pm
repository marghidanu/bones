package Bones::Skeleton::Python {
	use Moose;

	extends 'Bones::Skeleton';

	use Mojo::File qw( path );

	use Bones::Util qw( dist_dir );
	use Bones::Dialog;
	use Bones::Spine;

	with 'MooseX::Log::Log4perl';

	sub BUILD {
		my $self = shift();

		$self->add_task( dialog => [],
			sub {
				my ( $s, $c ) = @_;

				my $base = path( $c->global( 'path' ) );
				my $defaults = $c->get_result( 'dialog' ) || {};

				return ask(
					{
						type => 'text',
						name => 'name',
						title => 'what is the name of this module?',
						default => $defaults->{name} || $base->basename(),
					},
					{
						type => 'text',
						name => 'version',
						title => 'Do you have a version for it?',
						default => $defaults->{version} || '0.1',
					},
					{
						type => 'text',
						name => 'description',
						title => 'Do you have a description for it?',
						default => $defaults->{description} || 'None',
					},
					{
						type => 'text',
						name => 'author',
						title => 'Who is the author for this module?',
						default => $defaults->{author} || $c->global( 'author' ),
					}
				);
			}
		);

		$self->add_task( default => [ qw( dialog ) ],
			sub {
				my ( $s, $c ) = @_;

				my $answers = $c->get_result( 'dialog' );

				my $base = path( dist_dir() )
					->child( 'templates/python' )
					->to_string();

				my $script = path( 'bin' )
					->child( sprintf( '%s.py', $answers->{name} ) )
					->to_string();

				my $module = path( $answers->{name} )
					->child( '__init__.py' )
					->to_string();

				$answers->{script} = $script;

				my $spine = Bones::Spine->new( base => $base )
					->load(
						'script.ep' => $script,
						'init.ep' => $module,
						'setup.ep' => 'setup.py',
						'requirements.ep' => 'requirements.txt',
						'readme.ep' => 'README.md',
						'gitignore.ep' => '.gitignore',
					)
					->apply( template => { args => $answers } )
					->apply( save => {
							base => $c->global( 'path' ),
							overwrite => $c->global( 'overwrite' ),
						}
					)
					->apply( status => {} );

				return 0;
			}
		);

		return 0;
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
