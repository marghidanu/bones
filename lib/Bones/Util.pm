package Bones::Util {
	use Moose;
	use Moose::Exporter;

	use File::ShareDir;

	Moose::Exporter->setup_import_methods(
		as_is => [ qw( dist_file dist_dir ) ]
	);

	our $DIST = 'Bones';

	sub dist_file {
		return File::ShareDir::dist_file( $DIST, shift() );
	}

	sub dist_dir {
		return File::ShareDir::dist_dir( $DIST );
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
