package Bones::Spine::FilterFactory {
	use MooseX::AbstractFactory;

	use Mojo::Util qw( camelize );

	implementation_class_via sub {
		my $name = shift();

		return sprintf( 'Bones::Spine::Filter::%s', camelize( $name ) );
	};
}

1;

__END__
