package Bones::Spine::Block {
	use Moose;

	has 'name' => (
		is => 'ro',
		isa => 'Str',
		required => 1
	);

	has 'content' => (
		is => 'rw',
		isa => 'Maybe[Str]',
		default => undef,
	);

	sub get_size {
		my $self = shift();

		return length( $self->content() );
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
