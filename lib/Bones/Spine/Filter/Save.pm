package Bones::Spine::Filter::Save {
	use Moose;

	extends 'Bones::Spine::Filter';

	use Mojo::File qw( path );
	use Mojo::Util qw( md5_sum );

	use Term::Prompt qw( prompt );

	has 'base' => (
		is => 'ro',
		isa => 'Str',
		required => 1,
	);

	has 'overwrite' => (
		is => 'ro',
		isa => 'Bool',
		default => 0,
	);

	with 'MooseX::Log::Log4perl';

	sub apply {
		my ( $self, $block )  = @_;

		my $path = path( $self->base() )
			->child( $block->name() );

		if( -e $path && -f $path ) {
			$self->log()->warn( "File ${path} already exists!" ) && return
				unless( $self->overwrite() );

			$self->log()->warn( "File ${path} has no changes." ) && return
				if( md5_sum( $path->slurp() ) eq md5_sum( $block->content() ) );

			my $confirmation = prompt( 'y', "Do you want to replace file ${path}?", '', 'n' );
			return
				unless( $confirmation );
		}

		# TODO: Implement smart write using checksum
		$self->log()->info( "Writing file ${$path}" );
		$path->dirname()->make_path();
		$path->spurt( $block->content() );

		return 0;
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
