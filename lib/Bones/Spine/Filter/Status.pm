package Bones::Spine::Filter::Status {
	use Moose;

	extends 'Bones::Spine::Filter';

	use Number::Bytes::Human qw( format_bytes );

	has 'total_size' => (
		is => 'ro',
		isa => 'Int',
		default => 0,
		traits => [ qw( Number ) ],
		handles => {
			'add_to_size' => 'add',
		}
	);

	has 'counter' => (
		is => 'ro',
		isa => 'Int',
		default => 0,
		traits => [ qw( Counter ) ],
		handles => {
			'inc_counter' => 'inc',
		}
	);

	with 'MooseX::Log::Log4perl';

	sub apply {
		my ( $self, $block ) = @_;

		$self->add_to_size( $block->get_size() );
		$self->inc_counter();

		return 0;
	}

	override 'done' => sub {
		my $self = shift();

		$self->log()->info(
			sprintf( 'Processed %d files (%s bytes)',
				$self->counter(),
				format_bytes( $self->total_size() ),
			)
		);

		return 0;
	};

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
