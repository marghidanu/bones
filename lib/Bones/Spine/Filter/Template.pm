package Bones::Spine::Filter::Template {
	use Moose;

	extends 'Bones::Spine::Filter';

	use Carp;

	use Mojo::Template;

	has 'args' => (
		is => 'ro',
		isa => 'HashRef',
		default => sub { {} },
	);

	has '_engine' => (
		is => 'ro',
		isa => 'Mojo::Template',
		default => sub { Mojo::Template->new( vars => 1 ) }
	);

	with 'MooseX::Log::Log4perl';

	sub apply {
		my ( $self, $block ) = @_;

		$self->log()->info( sprintf( "Processing %s", $block->name() ) );
		my $result = $self->_engine()
			->render( $block->content(), $self->args() );

		croak( $result )
			if( ref( $result ) );

		$block->content( $result );

		return 0;
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
