package Bones::Spine::Filter {
	use Moose;

	use MooseX::AbstractMethod;

	abstract( 'apply' );

	sub done {}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
