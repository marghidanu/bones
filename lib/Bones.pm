package Bones {
	use MooseX::App qw( Version );

	our $VERSION = '0.0.0';

	app_namespace( 'Bones::Command' );
	app_permute( 1 );

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
