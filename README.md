# bones

[![pipeline status](https://gitlab.com/marghidanu/bones/badges/master/pipeline.svg)](https://gitlab.com/marghidanu/bones/commits/master)

## Description

![Bones](http://www.startrek.com/uploads/assets/db_articles/4a69dc066ecadefa4fa65c2ed38a55329aa4b104.jpg)

## Development environment

```bash
vagrant up
vagrant ssh

sudo su -
cd /vagrant

perl Build.PL
./Build installdeps
./Build
./Build test
```

## Installation

You can easily install the current master directly from GitLab:

```bash
cpanm --sudo --notest --quiet https://gitlab.com/marghidanu/bones.git
```
or you can install it form source

```bash
git clone https://gitlab.com/marghidanu/bones.git

cd bones
perl Build.PL
./Build installdeps
./Build
./Build test && sudo ./Build install
./Build distclean
```

This will install the application globally on your system.

## Uninstall

If you have an older version of **Bones** installed on your systems is recommended
to completly uninstall it before installing a new one.

```bash
cpanm --sudo --notest --quiet App::pmuninstall
sudo pm-uninstall -f Bones
```

## Manual

### Listing your skeletons

To get a list of the available skeletons on your system, simply type in the following Bones command.

```bash
bones.pl list
```

You can also list the tasks for all skeletons in the order that they will execute by adding the **verbose** parameter.

```bash
bones.pl list --verbose
```

### Running skeletons

Running a skeleton is easy, we just need to specify which skeleton we want to run and give it a location on disk where
to store the files. There are some additional parameters like **author** and **email** but those aren't mandatory.

```bash
bones.pl init --path ~/Documents/Projects/my-project \
	--skeleton perl
```

Running multiple skeletons at once:

```bash
bones.pl init --path ~/Documents/Projects/my-project \
	--skeleton perl vagrant license
```

#### Caching option

Bones keeps track of your answers when you create a project using multiple skeletons. In case you want to make use of your previous answers you can enable that by using the **cache** option.

```bash
bones.pl init --path ~/Documents/Projects/existing-project \
	--skeleton perl \
	--cache
```
The command above will pickup your answers from the previous session and use them as defaults for the current one.

**NOTES**:
* Menu values are not cached.
* It will NOT cleanup your directories and files, you will have to do that manually.

### Overwriting files

It is possible to overwrite files, but you will be prompted if the file has changes otherwise the file will be skipped or created (if the file doesn't exist already) automatically for you .

```bash
bones.pl init --path ~/Documents/Projects/existing-project \
	--skeleton perl \
	--overwrite \
	--cache
```

### Configuration

TODO

### Extending Bones

TODO

#### Adding a skeleton

TODO

#### Using Spine

TODO

#### Templates

TODO
