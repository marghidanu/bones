#!/usr/bin/env perl

package main {
	use Bones;
	use Bones::Util qw( dist_file );

	use Log::Log4perl;

	Log::Log4perl->init( dist_file( 'log4perl.conf' ) );

	Bones->new_with_command()
		->run();
}
