#!/usr/bin/env perl

use strict;
use warnings;

use Test::Perl::Critic (
	-severity => 3,
	-exclude => [
		qw(
			strict warnings
			ProhibitMultiplePackages ProhibitPostfixControls ProhibitEmptyQuotes
			ProhibitLongChainsOfMethodCalls RequireVersionVar
		)
	]
);

all_critic_ok();
