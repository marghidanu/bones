#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

BEGIN {
	use_ok( 'Bones::Skeleton' )
}

my $flow = Bones::Skeleton->new();
isa_ok( $flow, 'Bones::Skeleton' );
can_ok( $flow, qw( add_task ) );

$flow->add_task( first => [],
	sub {
		my ( $f, $c ) = @_;

		return $c->global( 'a' );
	}
);

$flow->add_task( second => [],
	sub {
		my ( $f, $c ) = @_;

		return $c->global( 'b' );
	}
);

$flow->add_task( third => [ qw( first second ) ],
	sub {
		my ( $f, $c ) = @_;

		return $c->get_result( 'first' ) + $c->get_result( 'second' );
	}
);

my $context = Bones::Skeleton::Context->new(
	globals => {
		a => 1,
		b => 2,
	}
);
isa_ok( $context, 'Bones::Skeleton::Context' );

$flow->run( $context );

done_testing();
