#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

BEGIN {
	use_ok( 'Bones::Dialog', qw( ask ) );
}

done_testing();
