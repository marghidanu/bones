#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

BEGIN {
	use_ok( 'Bones::Spine' );
	use_ok( 'Bones::Spine::Filter::Template' );
	use_ok( 'Bones::Spine::Filter::Save' );

	use_ok( 'Bones::Util' );
}

my $instance = Bones::Spine->new();
isa_ok( $instance, 'Bones::Spine' );
can_ok( $instance, qw( load apply ) );

# my $args = {
# 	name => 'x',
# 	secret => 'xxx',
# };
#
# $instance
# 	->load( dist_file( 'test1.ep' ), 'other/secret.txt' )
# 	->load( dist_file( 'test2.ep' ), 'name.txt' )
# 	->apply( template( $args ) )
# 	->apply( save( '/opt/spine', { overwrite => 1 } ) );

done_testing();
